<?php
return [
    'interest_rate' => 5,
    'app_type' => env('APP_TYPE'),
    'loan_status' => [
        'Applied',
        'Approved',
        'Rejected',
        'Completed'
    ],
    'loan_completed' => [
        'No',
        'Yes',
    ]

];

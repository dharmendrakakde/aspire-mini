<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLoanApplication extends Model
{
    use HasFactory, SoftDeletes;

    const STATUS_COMPLETED_YES = 1, STATUS_COMPLETED_NO = 0;

    const STATUS_LOAN_REQUESTED = 0, STATUS_LOAN_APPROVED = 1, STATUS_LOAN_REJECTED = 2, STATUS_LOAN_COMPLETED = 3;

    protected $fillable = [
        'user_id', 'amount', 'term', 'loan_status', 'is_completed',
        'interest_rate', 'weekly_repay_amount', 'amount_left'
    ];

    public function getInterestRate()
    {
        if (isset($this->interest_rate)) {
            return $this->interest_rate;
        } else {
            return config('default.interest_rate');
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function installments()
    {
        return $this->hasMany(LoanPayment::class, 'loan_id','id');
    }
}

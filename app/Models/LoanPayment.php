<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanPayment extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'loan_id', 'amount'
    ];

    public function loan()
    {
        return $this->belongsTo(UserLoanApplication::class, 'loan_id', 'id');
    }
}

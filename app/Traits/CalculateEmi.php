<?php

namespace App\Traits;


trait CalculateEmi
{
    public function calculateEmi($amount, $rate, $term)
    {
        // Here interest we will take per periodic installment
        $interest = $rate / (52 * 100); // here 52 is a week

        $emiAmount = $interest * -$amount * pow((1 + $interest), $term) / (1 - pow((1 + $interest), $term));
        return sprintf("%.02f", $emiAmount);
    }
}

<?php

use \App\Models\UserLoanApplication;

if (!function_exists('getAppliedLoanRate')) {
    function getAppliedLoanRate($loanId)
    {
        $userLoanApplication = UserLoanApplication::find($loanId);

        $rate = (!empty($userLoanApplication)) ? $userLoanApplication->getInterestRate() : (new UserLoanApplication())->getInterestRate();

        return $rate;
    }
}


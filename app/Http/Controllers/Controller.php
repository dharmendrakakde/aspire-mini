<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    const SUCCESS = 200, BAD_REQUEST = 400, UNAUTHORIZED = 401, NOT_FOUND = 404, VALIDATION_ERROR = 422, SERVER_ERROR = 500;


    /**
     * @param $status
     * @param null $message
     * @param null $data
     * @param array $otherData
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function apiResponse($status, $message = null, $data = null, array $otherData = [], $statusCode = 200)
    {
        return response()->json(
            array_merge([
                'status' => $status,
                'message' => $message,
                'data' => $data,
            ], $otherData),
            $statusCode
        );
    }
}

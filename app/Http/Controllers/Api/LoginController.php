<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {

        $input = $request->only('name', 'email', 'password');

        $validator = Validator::make($input, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:15'
        ]);

        if ($validator->fails()) {
            return $this->apiResponse(false, trans('api_lang.validation_error'), $validator->messages(), [], $this::VALIDATION_ERROR);
        }

        try {
            DB::beginTransaction();

            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);

            $credentials = $request->only('email', 'password');

            $token = JWTAuth::attempt($credentials);

            DB::commit();

            return $this->apiResponse(true, trans('api_lang.register_successfully'), [
                'token' => $token,
                'user' => Auth::user(),
            ], []);

        } catch (\Exception $e) {
            return $this->apiResponse(false, trans('api_lang.something_went_wrong'), [], [], $this::SERVER_ERROR);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:15'
        ]);

        if ($validator->fails()) {
            return $this->apiResponse(false, trans('api_lang.validation_error'), $validator->messages(), [], $this::VALIDATION_ERROR);
        }

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->apiResponse(false, trans('api_lang.invalid_credentials'), [], [], $this::BAD_REQUEST);
            }
            $user = Auth::user();
        } catch (JWTException $e) {
            return $this->apiResponse(false, trans('api_lang.could_not_create_token'), [], [], $this::SERVER_ERROR);
        }

        return $this->apiResponse(true, trans('api_lang.login_successfully'), [
            'token' => $token,
            'user' => $user,
        ], []);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        try {
            JWTAuth::invalidate($request->header('Authorization'));
            return $this->apiResponse(true, trans('api_lang.logout_successfully'), [], []);
        } catch (JWTException $exception) {
            return $this->apiResponse(false, trans('api_lang.user_can_not_be_logout'), [], [], $this::SERVER_ERROR);
        }
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return $this->apiResponse(false, trans('api_lang.user_not_found'), [], [], $this::NOT_FOUND);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return $this->apiResponse(false, trans('api_lang.token_is_expired'), [], [], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return $this->apiResponse(false, trans('api_lang.token_is_invalid'), [], [], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return $this->apiResponse(false, trans('api_lang.token_absent'), [], [], $e->getStatusCode());

        }
        return $this->apiResponse(true, trans('api_lang.success'), [
            'user' => $user,
        ], []);
    }
}

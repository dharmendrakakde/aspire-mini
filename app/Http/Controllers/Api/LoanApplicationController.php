<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LoanPayment;
use App\Models\UserLoanApplication;
use App\Traits\CalculateEmi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LoanApplicationController extends Controller
{
    use CalculateEmi;

    /**
     * User apply for a loan
     * @param Request $request
     */
    public function apply(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'amount' => 'required|min:1',
                'term' => 'required|min:1',
            ]);

            if ($validator->fails()) {
                return $this->apiResponse(false, trans('api_lang.validation_error'), $validator->messages(), [], $this::VALIDATION_ERROR);
            }

            DB::beginTransaction();

            $userApplication = new UserLoanApplication();

            $loanInterest = $userApplication->getInterestRate();
            $amount = $request->amount;
            $term = $request->term;

            $getEmi = $this->calculateEmi($amount, $loanInterest, $term);

            $userApplication->user_id = Auth::user()->id;
            $userApplication->amount = $amount;
            $userApplication->term = $term;
            $userApplication->interest_rate = $loanInterest;
            $userApplication->weekly_repay_amount = $getEmi;
            $userApplication->amount_left = $getEmi * $term;
            $userApplication->loan_status = UserLoanApplication::STATUS_LOAN_REQUESTED;
            $userApplication->is_completed = UserLoanApplication::STATUS_COMPLETED_NO;
            $userApplication->save();

            DB::commit();

            return $this->apiResponse(true, trans('api_lang.loan_applied_successfully'), [
                'loan_id' => $userApplication->id,
                'loan_info' => $userApplication,
                'installment_per_week' => $getEmi,
            ], []);

        } catch (\Exception $e) {
            DB::rollBack();
            if (config('default.app_type') == "local") {
                return $this->apiResponse(false, trans('api_lang.error'), $e->getMessage(), [], $this::SERVER_ERROR);
            }

            return $this->apiResponse(false, trans('api_lang.something_went_wrong'), [], [], $this::SERVER_ERROR);
        }
    }


    /**
     * Installment Calculation
     * @param Request $request
     * @param null $loanId
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateInstallment(Request $request, $loanId = null)
    {
        try {

            if ($loanId == null) {
                $validator = Validator::make($request->all(), [
                    'amount' => 'required|min:1',
                    'term' => 'required|min:1',
                    'interest_rate' => 'required|min:1',
                ]);

                if ($validator->fails()) {
                    return $this->apiResponse(false, trans('api_lang.validation_error'), $validator->messages(), [], $this::VALIDATION_ERROR);
                }
            } else {
                $validator = Validator::make($request->all(), [
                    'amount' => 'required|min:1',
                    'term' => 'required|min:1',
                ]);

                if ($validator->fails()) {
                    return $this->apiResponse(false, trans('api_lang.validation_error'), $validator->messages(), [], $this::VALIDATION_ERROR);
                }
            }

            $amount = $request->amount;
            $term = $request->term;

            if ($loanId != null) {
                $interestRate = getAppliedLoanRate($loanId);
            } else {
                $interestRate = $request->interest_rate;
            }

            $installmentAmountPerWeek = $this->calculateEmi($amount, $interestRate, $term);

            $totalInterestPayable = number_format((float)($installmentAmountPerWeek * $term) - $amount, 2, '.', '');

            $totalPayment = $amount + $totalInterestPayable;

            return $this->apiResponse(true, trans('api_lang.installment_calculated_successfully'), [
                'term' => $term,
                'amount' => $amount,
                'interest_rate' => $interestRate,
                'installment_amount' => $installmentAmountPerWeek,
                'total_interest_payable' => $totalInterestPayable,
                'total_payment' => $totalPayment,
            ], []);

        } catch (\Exception $e) {
            if (config('default.app_type') == "local") {
                return $this->apiResponse(false, trans('api_lang.error'), $e->getMessage(), [], $this::SERVER_ERROR);
            }
            return $this->apiResponse(false, trans('api_lang.something_went_wrong'), [], [], $this::SERVER_ERROR);

        }
    }

    /**
     * Approve user loan application
     * @param $id (Loan Application ID)
     */
    public function approveLoanApplication($id)
    {
        try {

            $userLoanApplication = UserLoanApplication::find($id);

            if (!$userLoanApplication instanceof UserLoanApplication) {
                return $this->apiResponse(false, trans('api_lang.application_not_found'), [], [], $this::NOT_FOUND);
            }

            if ($userLoanApplication->loan_status == UserLoanApplication::STATUS_LOAN_REQUESTED) {
                DB::beginTransaction();

                $userLoanApplication->loan_status = UserLoanApplication::STATUS_LOAN_APPROVED;
                $userLoanApplication->save();

                DB::commit();
                return $this->apiResponse(true, trans('api_lang.loan_approved_successfully'), [
                    'user_loan_application' => $userLoanApplication,
                ], []);

            }

            return $this->apiResponse(true, trans('api_lang.only_requested_loan_will_be_approved'), [], []);

        } catch (\Exception $e) {
            DB::rollBack();
            if (config('default.app_type') == "local") {
                return $this->apiResponse(false, trans('api_lang.error'), $e->getMessage(), [], $this::SERVER_ERROR);
            }

            return $this->apiResponse(false, trans('api_lang.something_went_wrong'), [], [], $this::SERVER_ERROR);
        }
    }

    /**
     * Reject user loan application
     * @param $id (Loan Application ID)
     */
    public function rejectApplication($id)
    {
        try {

            $userLoanApplication = UserLoanApplication::find($id);

            if (!$userLoanApplication instanceof UserLoanApplication) {
                return $this->apiResponse(false, trans('api_lang.application_not_found'), [], [], $this::NOT_FOUND);
            }

            if ($userLoanApplication->loan_status == UserLoanApplication::STATUS_LOAN_REQUESTED) {
                DB::beginTransaction();

                $userLoanApplication->loan_status = UserLoanApplication::STATUS_LOAN_REJECTED;
                $userLoanApplication->save();

                DB::commit();
                return $this->apiResponse(true, trans('api_lang.loan_reject_successfully'), [
                    'user_loan_application' => $userLoanApplication,
                ], []);

            }

            return $this->apiResponse(true, trans('api_lang.only_requested_loan_will_be_rejected'), [], []);

        } catch (\Exception $e) {
            DB::rollBack();
            if (config('default.app_type') == "local") {
                return $this->apiResponse(false, trans('api_lang.error'), $e->getMessage(), [], $this::SERVER_ERROR);
            }

            return $this->apiResponse(false, trans('api_lang.something_went_wrong'), [], [], $this::SERVER_ERROR);
        }
    }


    /**
     * Get current logged in user loans
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserLoans()
    {
        try {
            $userApplications = UserLoanApplication::where('user_id', Auth::user()->id)->get();

            return $this->apiResponse(true, trans('api_lang.data_received_successfully'), [
                'user_loan_application' => $userApplications,
            ], []);

        } catch (\Exception $e) {
            if (config('default.app_type') == "local") {
                return $this->apiResponse(false, trans('api_lang.error'), $e->getMessage(), [], $this::SERVER_ERROR);
            }
            return $this->apiResponse(false, trans('api_lang.something_went_wrong'), [], [], $this::SERVER_ERROR);
        }
    }


    /**
     * Pay EMI
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function payEmi(Request $request, $id)
    {
        try {

            $application = UserLoanApplication::with('installments')->find($id);

            if (!$application instanceof UserLoanApplication) {
                return $this->apiResponse(false, trans('api_lang.loan_application_not_found'), [], [], $this::NOT_FOUND);
            }

            if ($application->loan_status == UserLoanApplication::STATUS_LOAN_APPROVED && $application->is_completed == UserLoanApplication::STATUS_COMPLETED_NO) {
                $repayAmount = $request->has('payment') ? $request->payment : $application->weekly_repay_amount;

                if ($application->amount_left < $repayAmount) {
                    return $this->apiResponse(true, sprintf('Your loan amount is left %.02f and trying to pay amount %.02f', $application->amount_left, $repayAmount), [], []);
                }
                LoanPayment::create([
                    'loan_id' => $id,
                    'amount' => $repayAmount,
                ]);

                $application->amount_left = $application->amount_left - $repayAmount;
                if ($application->amount_left <= 0) {
                    $application->is_completed = UserLoanApplication::STATUS_COMPLETED_YES;
                    $application->loan_status = UserLoanApplication::STATUS_LOAN_COMPLETED;
                }
                $application->save();

                $userLoanApplication = UserLoanApplication::with('installments')->find($id);

                return $this->apiResponse(true, trans('api_lang.data_received_successfully'), [
                    'user_loan_application' => $userLoanApplication,
                ], []);

            }

            return $this->apiResponse(false, trans('api_lang.payment_is_not_accepted'), [], [], $this::NOT_FOUND);

        } catch (\Exception $e) {
            if (config('default.app_type') == "local") {
                return $this->apiResponse(false, trans('api_lang.error'), $e->getMessage(), [], $this::SERVER_ERROR);
            }
            return $this->apiResponse(false, trans('api_lang.something_went_wrong'), [], [], $this::SERVER_ERROR);
        }
    }


    /**
     * Get loan info
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function loanInfo($id)
    {
        try {
            
            $userLoanApplication = UserLoanApplication::with('user', 'installments')->find($id);

            if ($userLoanApplication instanceof UserLoanApplication) {
                return $this->apiResponse(true, trans('api_lang.data_received_successfully'), [
                    'user_loan_application' => $userLoanApplication,
                ], []);
            }

            return $this->apiResponse(false, trans('api_lang.payment_is_not_accepted'), [], [], $this::NOT_FOUND);

        } catch (\Exception $e) {
            if (config('default.app_type') == "local") {
                return $this->apiResponse(false, trans('api_lang.error'), $e->getMessage(), [], $this::SERVER_ERROR);
            }
            return $this->apiResponse(false, trans('api_lang.something_went_wrong'), [], [], $this::SERVER_ERROR);
        }
    }
}

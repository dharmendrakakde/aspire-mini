<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\LoanApplicationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [LoginController::class, 'authenticate']);

Route::post('register', [LoginController::class, 'register']);

Route::group(['middleware' => ['jwt.verify']], function () {

    Route::get('get-user', [LoginController::class, 'getAuthenticatedUser']);

    Route::controller(LoanApplicationController::class)->group(function () {

        Route::group(['prefix' => 'loan'], function() {

            Route::post('apply', 'apply');
            Route::post('calculate/{loanId?}', 'calculateInstallment');

            Route::get('{id}', 'loanInfo');
            Route::post('{id}/approve', 'approveLoanApplication');
            Route::post('{id}/reject', 'rejectApplication');
            Route::post('{id}/pay-emi', 'payEmi');

        });

        Route::get('get-user-loan', 'getUserLoans');
    });

    Route::get('logout', [LoginController::class, 'logout']);
});

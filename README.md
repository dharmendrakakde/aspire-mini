# README #
Aspire Mini Loan Calculator (API). 
Laravel Framework version 8.83.0

### System Requirements ###
* PHP >= 7.3
* BCMath PHP Extension
* Ctype PHP Extension
* Fileinfo PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

### How do I get set up? ###

* clone the project
* composer install
* set-up .env
* php artisan key:generate
* php artisan migrate
* php artisan optimize:clear

### Postman collection link ###

* Steps
    * Open postman
    * Click on Import button
    * Click on Link button
    * Paste this URL 
        https://www.getpostman.com/collections/76daa890819cd015f58d
    * Click on Continue button
    
* https://www.getpostman.com/collections/76daa890819cd015f58d

### API documentation link ###

Please check this documentation. Some parameters are optional and some are required.

* https://docs.google.com/document/d/1eaFUKBu43YWgnxTcPw-lU2ZD7B-9yGehqt0reqnKQPA/edit?usp=sharing

### Who do I talk to? ###

* Dharmendra Kakde - dharmendrakakde@gmail.com
